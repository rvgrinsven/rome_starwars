export default function(species, selectedFilm) {
  return species.filter(
    species => !selectedFilm || species.films.indexOf(selectedFilm) > -1
  );
}
