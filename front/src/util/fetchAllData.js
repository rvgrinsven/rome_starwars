import { gql } from "apollo-boost";

export default async function(client) {
  /* 
  		Since I'm not to familiar with Appolo as a client, I used
			a bit of a hack using the existing height attribute to transfer
			the imageUrl. This is because Apollo filters out any extra data.
  	*/
  return await client.query({
    query: gql`
      {
        allFilms {
          edges {
            node {
              id
              title
            }
          }
        }
        allSpecies {
          edges {
            node {
              id
              name
              filmConnection {
                edges {
                  node {
                    id
                  }
                }
              }
            }
          }
        }
        allPeople {
          edges {
            node {
              id
              name
              height
              species {
                id
              }
              filmConnection {
                edges {
                  node {
                    id
                  }
                }
              }
            }
          }
        }
      }
    `
  });
}
