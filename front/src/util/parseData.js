export default function(rawData) {
  const films = rawData.data.allFilms.edges.map(film => ({
    id: film.node.id,
    title: film.node.title
  }));
  const species = rawData.data.allSpecies.edges.map(specie => ({
    id: specie.node.id,
    name: specie.node.name,
    films: specie.node.filmConnection.edges.map(film => film.node.id)
  }));
  const people = rawData.data.allPeople.edges.map(person => ({
    id: person.node.id,
    name: person.node.name,
    species: person.node.species != null ? person.node.species.id : null,
    films: person.node.filmConnection.edges.map(film => film.node.id),
    imageUrl: person.node.height
  }));

  return { films, species, people };
}
