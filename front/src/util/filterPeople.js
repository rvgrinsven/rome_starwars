export default function(people, selectedFilm, selectedSpecies) {
  return people.filter(person => {
    if (selectedFilm && person.films.indexOf(selectedFilm) == -1) {
      return false;
    }
    if (
      selectedSpecies &&
      (person.species == null || selectedSpecies != person.species)
    ) {
      return false;
    }
    return true;
  });
}
