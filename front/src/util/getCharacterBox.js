import React from "react";
import CharacterBox from "../CharacterBox";

export default function(selectedPerson, films, selectedFilm) {
  /*
    If there is a film selected, pick the selected film, 
    otherwise pick a random film the person stars in.
  */
  const filmToShow = films.filter(film =>
    selectedFilm
      ? film.id == selectedFilm
      : selectedPerson.films.indexOf(film.id) > -1
  )[0];
  return (
    <CharacterBox
      key={selectedPerson.id}
      characterName={selectedPerson.name}
      movieName={filmToShow.title}
      imageUrl={selectedPerson.imageUrl}
    />
  );
}
