import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

const Wrapper = styled.div`
  width: 200px;
  height: 360px;
  margin-left: 15px;
  margin-right: 15px;
  margin-top: 30px;
  margin-bottom: 30px;
  box-shadow: 0 0 15px white;
`;

const TextContainer = styled.div`
  height: 60px;
  background-color: white;
  padding-top: 10px;
  padding-left: 10px;
  font-family: sans-serif;
`;

const Title = styled.div`
  font-size: 1.5em;
`;

const SubTitle = styled.div`
  margin-top: '2px'
  font-size: 0.2em;
`;

class CharacterBox extends React.Component {
  render() {
    const { characterName, movieName, imageUrl } = this.props;
    return (
      <Wrapper>
        <img src={imageUrl} width="200px" height="300px" />
        <TextContainer>
          <Title>{characterName}</Title>
          <SubTitle>{movieName}</SubTitle>
        </TextContainer>
      </Wrapper>
    );
  }
}

export default CharacterBox;
