import React from "react";
import ReactDOM from "react-dom";
import ApolloClient from "apollo-boost";
import styled from "styled-components";
import SelectBox from "./SelectBox";
import CharacterBox from "./CharacterBox";
import filterPeople from "./util/filterPeople";
import filterSpecies from "./util/filterSpecies";
import fetchAllData from "./util/fetchAllData";
import parseData from "./util/parseData";
import getCharacterBox from "./util/getCharacterBox";

const SelectContainer = styled.div`
  width: 70%;
  margin-top: 140px;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  flex-wrap: wrap;
  padding: 5px;
  justify-content: space-evenly;
`;

const CharacterBoxContainer = styled.div`
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 30px;
  display: flex;
  flex-wrap: wrap;
  padding: 5px;
  justify-content: space-evenly;
`;

class App extends React.Component {
  constructor(props) {
    const client = new ApolloClient({
      uri: "http://127.0.0.1:8087/"
    });

    super(props);
    this.state = {
      client,
      films: [],
      species: [],
      people: [],
      selectedFilm: null,
      selectedSpecies: null,
      selectedPerson: null
    };
  }

  async componentDidMount() {
    /*
	  	We are fetching and converting all the data at the start, 
	  	since we have to start off showing all the films anyway, 
	  	this is the most efficient.
    */
    const response = await fetchAllData(this.state.client);
    const { films, species, people } = parseData(response);
    await this.setState({ films, species, people });
  }

  render() {
    /*
      Below werender the 3 select boxes and all necessary Character boxes.
      Filtering happens on render, this could be cached but since it's such
      a small dataset it hardly matters.
    */
    return [
      <SelectContainer>
        <SelectBox
          labelField="title"
          items={this.state.films}
          selectedId={this.state.selectedFilm}
          updateField={e =>
            this.setState({
              selectedFilm: e.target.value,
              selectedSpecies: 0,
              selectedPerson: 0
            })
          }
        />
        <SelectBox
          items={filterSpecies(this.state.species, this.state.selectedFilm)}
          selectedId={this.state.selectedSpecies}
          updateField={e =>
            this.setState({
              selectedSpecies: e.target.value,
              selectedPerson: 0
            })
          }
        />
        <SelectBox
          items={filterPeople(
            this.state.people,
            this.state.selectedFilm,
            this.state.selectedSpecies
          )}
          selectedId={this.state.selectedPerson}
          updateField={e => this.setState({ selectedPerson: e.target.value })}
        />
      </SelectContainer>,
      <CharacterBoxContainer>
        {/*
          If a single person is selected return him, 
          if not return all filtered people
        */
        this.state.selectedPerson
          ? getCharacterBox(
              this.state.people.filter(
                person => person.id == this.state.selectedPerson
              )[0],
              this.state.films,
              this.state.selectedFilm
            )
          : filterPeople(
              this.state.people,
              this.state.selectedFilm,
              this.state.selectedSpecies
            ).map(person =>
              getCharacterBox(person, this.state.films, this.state.selectedFilm)
            )}
      </CharacterBoxContainer>
    ];
  }
}

ReactDOM.render(<App />, document.querySelector("#main"));
