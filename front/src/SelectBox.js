import React from "react";
import styled from "styled-components";

const StyledSelect = styled.select`
  width: 250px;
  height: 14vh;
  max-height: 40px;
  margin-top: "20px";
  margin-bottom: 20px;
  margin-left: 20px;
  margin-right: 20px;
`;

class SelectBox extends React.PureComponent {
  render() {
    const {
      labelField,
      valueField,
      items,
      selectedId,
      updateField
    } = this.props;
    return (
      <StyledSelect value={selectedId} onChange={updateField}>
        <option key={0} value={""}></option>
        {items.map(item => (
          <option key={item[valueField]} value={item[valueField]}>
            {item[labelField]}
          </option>
        ))}
      </StyledSelect>
    );
  }
}

SelectBox.defaultProps = {
  labelField: "name",
  valueField: "id"
};

export default SelectBox;
