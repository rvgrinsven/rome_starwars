var webpack = require('webpack');
var path = require('path');
const prod = process.argv.indexOf('-p') !== -1;

var config = {
	entry: [
		'./src/App.js'
	],
	module: {
		rules: [
			{
		      test: /\.m?jsx?$/,
      		  exclude: /node_modules/,
		      use: {
		        loader: 'babel-loader',
		        options: {
		          cacheDirectory: true
		        }
		      }
		    }
		]
	},
	resolve: {
		extensions: [".json", ".js", ".jsx"]
	},
	plugins: [
    	new webpack.ProvidePlugin({
    	  'fs': null 
		})
	],
	node: {
   		fs: "empty"
	}
};



module.exports = config;