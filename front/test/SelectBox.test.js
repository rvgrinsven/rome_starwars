import React from "react";
import renderer from "react-test-renderer";
import SelectBox from "../src/SelectBox";

it("renders correctly", () => {
  const tree = renderer
    .create(<SelectBox items={[]} selectedId={0} updateField={e => {}} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
