import React from "react";
import renderer from "react-test-renderer";
import getCharacterBox from "../src/util/getCharacterBox";

it("renders correctly", () => {
  const tree = renderer
    .create(
      getCharacterBox(
        {
          id: "cGVvcGxlOjE=",
          name: "Luke Skywalker",
          species: null,
          films: ["ZmlsbXM6Mg=="],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/1.jpg"
        },
        [
          { id: "ZmlsbXM6MQ==", title: "A New Hope" },
          { id: "ZmlsbXM6Mg==", title: "The Empire Strikes Back" }
        ],
        "ZmlsbXM6Mg=="
      )
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
