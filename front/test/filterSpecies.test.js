import filterSpecies from "../src/util/filterSpecies";

test("returns empty when empty object supplied", () => {
  expect(filterSpecies([], 0)).toStrictEqual([]);
});

test("returns all records when no film set", () => {
  expect(
    filterSpecies(
      [
        {
          id: "c3BlY2llczox",
          name: "Human",
          films: ["1", "2"]
        },
        {
          id: "c3BlY2llczoy",
          name: "Droid",
          films: ["3", "4"]
        }
      ],
      ""
    )
  ).toEqual([
    {
      id: "c3BlY2llczox",
      name: "Human",
      films: ["1", "2"]
    },
    {
      id: "c3BlY2llczoy",
      name: "Droid",
      films: ["3", "4"]
    }
  ]);
});

test("Doesn't return records for the wrong film", () => {
  expect(
    filterSpecies(
      [
        {
          id: "c3BlY2llczox",
          name: "Human",
          films: ["1", "2"]
        },
        {
          id: "c3BlY2llczoy",
          name: "Droid",
          films: ["3", "4"]
        }
      ],
      "1"
    )
  ).toEqual([
    {
      id: "c3BlY2llczox",
      name: "Human",
      films: ["1", "2"]
    }
  ]);
});
