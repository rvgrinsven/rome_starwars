import filterPeople from "../src/util/filterPeople";

test("returns empty when empty object supplied", () => {
  expect(filterPeople([], 0, 0)).toStrictEqual([]);
});

test("returns all records when no film and species set", () => {
  expect(
    filterPeople(
      [
        {
          id: "cGVvcGxlOjE=",
          name: "Luke Skywalker",
          species: null,
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/1.jpg"
        },
        {
          id: "cGVvcGxlOjI=",
          name: "C-3PO",
          species: "c3BlY2llczoy",
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/2.jpg"
        },
        {
          id: "cGVvcGxlOjM=",
          name: "R2-D2",
          species: "c3BlY2llczoy",
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/3.jpg"
        }
      ],
      "",
      ""
    )
  ).toEqual([
    {
      id: "cGVvcGxlOjE=",
      name: "Luke Skywalker",
      species: null,
      films: [1, 2],
      imageUrl: "https://starwars-visualguide.com/assets/img/characters/1.jpg"
    },
    {
      id: "cGVvcGxlOjI=",
      name: "C-3PO",
      species: "c3BlY2llczoy",
      films: [1, 2],
      imageUrl: "https://starwars-visualguide.com/assets/img/characters/2.jpg"
    },
    {
      id: "cGVvcGxlOjM=",
      name: "R2-D2",
      species: "c3BlY2llczoy",
      films: [1, 2],
      imageUrl: "https://starwars-visualguide.com/assets/img/characters/3.jpg"
    }
  ]);
});

test("Doesn't return a records for the wrong film", () => {
  expect(
    filterPeople(
      [
        {
          id: "cGVvcGxlOjE=",
          name: "Luke Skywalker",
          species: null,
          films: [4],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/1.jpg"
        },
        {
          id: "cGVvcGxlOjI=",
          name: "C-3PO",
          species: "c3BlY2llczoy",
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/2.jpg"
        },
        {
          id: "cGVvcGxlOjM=",
          name: "R2-D2",
          species: "c3BlY2llczoy",
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/3.jpg"
        }
      ],
      1,
      ""
    )
  ).toEqual([
    {
      id: "cGVvcGxlOjI=",
      name: "C-3PO",
      species: "c3BlY2llczoy",
      films: [1, 2],
      imageUrl: "https://starwars-visualguide.com/assets/img/characters/2.jpg"
    },
    {
      id: "cGVvcGxlOjM=",
      name: "R2-D2",
      species: "c3BlY2llczoy",
      films: [1, 2],
      imageUrl: "https://starwars-visualguide.com/assets/img/characters/3.jpg"
    }
  ]);
});

test("Doesn't return a records for the wrong film and species", () => {
  expect(
    filterPeople(
      [
        {
          id: "cGVvcGxlOjE=",
          name: "Luke Skywalker",
          species: null,
          films: [4],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/1.jpg"
        },
        {
          id: "cGVvcGxlOjI=",
          name: "C-3PO",
          species: "c3BlY2llczoy",
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/2.jpg"
        },
        {
          id: "cGVvcGxlOjM=",
          name: "R2-D2",
          species: "asdasdasdas",
          films: [1, 2],
          imageUrl:
            "https://starwars-visualguide.com/assets/img/characters/3.jpg"
        }
      ],
      1,
      "c3BlY2llczoy"
    )
  ).toEqual([
    {
      id: "cGVvcGxlOjI=",
      name: "C-3PO",
      species: "c3BlY2llczoy",
      films: [1, 2],
      imageUrl: "https://starwars-visualguide.com/assets/img/characters/2.jpg"
    }
  ]);
});
