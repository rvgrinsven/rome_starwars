import parseData from "../src/util/parseData";

test("Converts the data correctly", () => {
  expect(
    parseData(
      {
        data: {
          allFilms: {
            edges: [
              {
                node: {
                  id: "ZmlsbXM6MQ==",
                  title: "A New Hope",
                  __typename: "Film"
                }
              }
            ]
          },
          allSpecies: {
            edges: [
              {
                node: {
                  filmConnection: {
                    edges: [
                      {
                        node: {
                          id: 1
                        }
                      },
                      {
                        node: {
                          id: 2
                        }
                      }
                    ]
                  },
                  id: "c3BlY2llczox",
                  name: "Human",
                  __typename: "Species"
                }
              }
            ]
          },
          allPeople: {
            edges: [
              {
                node: {
                  filmConnection: {
                    edges: [
                      {
                        node: {
                          id: 1
                        }
                      },
                      {
                        node: {
                          id: 2
                        }
                      }
                    ]
                  },
                  height:
                    "https://starwars-visualguide.com/assets/img/characters/1.jpg",
                  id: "cGVvcGxlOjE=",
                  name: "Luke Skywalker",
                  species: null,
                  __typename: "Person"
                }
              }
            ]
          }
        }
      },
      0,
      0
    )
  ).toEqual({
    films: [{ id: "ZmlsbXM6MQ==", title: "A New Hope" }],
    species: [{ id: "c3BlY2llczox", films: [1, 2], name: "Human" }],
    people: [
      {
        id: "cGVvcGxlOjE=",
        name: "Luke Skywalker",
        species: null,
        films: [1, 2],
        imageUrl: "https://starwars-visualguide.com/assets/img/characters/1.jpg"
      }
    ]
  });
});
