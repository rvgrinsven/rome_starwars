const fetch = require('node-fetch');

module.exports = async function(){
	let people = {};
	let pageNumber = 1;
	while(true){
		const rawResponse = await fetch('https://swapi.co/api/people/?page='+pageNumber)
    	if(rawResponse.status != 200){
    		break;
    	}
    	const json = await rawResponse.json();
    	
    	people.push
    	people = {...people, ...json.results.reduce((carry, character) => 
    		{carry[character.name] = "https://starwars-visualguide.com/assets/img/characters/"+character.url.replace(/[^\d]*/g,"")+".jpg"; 
    		return carry;
    	} , {})}
    	pageNumber++;
	}
	return people;
}