const path = require("path");
var express = require('express');
var proxy = require('http-proxy-middleware');
var proxyApp = express();
const scrapeImages = require("./src/scrapeImages");

/*
Since the specifications say to "find" the images, I will just
fetch all of them when the script starts. More convenient that way.
*/

async function main(){
	console.log("Loading the Star Wars images in advance...");
	const imageLookup = await scrapeImages();
	console.log("Done Loading the Star Wars images.");

	/* 
		We proxy the api below in express middleware. We intercept the 
		response, parse the JSON, and use the imagesLookup to add the 
		image URL to all people we can find. (We pass this as the height 
		attribute, that's a dirty hack because I couldn't figure out how 
		to prevent Appollo from filtering out attributes that are not in 
		the graphql schema.  
	*/

	const proxyFunction = proxy({ target: 'https://swapi.apis.guru', changeOrigin: true, onProxyRes : async function(proxyRes, req, res){
	      const _write = res.write;
	      const _end = res.end;
	      var output;
	      var body = "";
	      proxyRes.on('data', function(data) {
	          data = data.toString('utf-8');
	          body += data;
	      });

	      /*
					We overwrite the write method, this means we lose buffering, 
					but who cares, it's fast enough.
	      */
	      res.write = function (data) {}

	      res.end = function(){
	      	let output = "";
	      	jsonReturn = JSON.parse(body);
	
	      	jsonReturn.data.allPeople.edges = jsonReturn.data.allPeople.edges.map(person => ({
	      		...person,
	      		node: {
	      			...person.node,
	      			height: imageLookup[person.node.name]? imageLookup[person.node.name]: "/assets/questionmark.svg"
	      		}
	      	}));

	      	output = JSON.stringify(jsonReturn);
			res.setHeader( 'content-length', output.length + 4 );
			_end.apply( res, [ output ] );
	      }

	    } });


	proxyApp.post(
	  '/',
	  proxyFunction
	);

	proxyApp.get('/assets/:file', function (req, res) {
	  //res.set('Content-Encoding', 'gzip');  
	  res.sendFile(path.join(__dirname,"/../front/public/assets/",req.params.file));
	});

	proxyApp.get('/dist/:file', function (req, res) {
	  //res.set('Content-Encoding', 'gzip');  
	  res.sendFile(path.join(__dirname,"/../front/dist/",req.params.file));
	});

	proxyApp.get(['/','/index.html'], function (req, res) {  
	    res.sendFile(path.join(__dirname,"/../front/public/index.html"));
	});

	proxyApp.listen(8087);
	console.log("Listening on port 8087");
	console.log("Go to http://127.0.0.1:8087 in your browser");

}

main();